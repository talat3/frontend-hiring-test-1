import { getCookie, hasCookie } from "cookies-next";
import { GetServerSideProps, NextPage } from "next";
import React from "react";
import Calls from "../../collection/Calls/Calls";
import { CallPageProps } from "../../libs/types/pages";

const CallPage:NextPage<CallPageProps> = ({CallsRecord}) => {
  return <Calls CallsRecord={CallsRecord}/>;
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  if (hasCookie("UserToken",context)) {
  let gettoken: any =getCookie("UserToken",context)
  gettoken=(JSON.parse(gettoken).token)
  const response = await fetch(
    `https://frontend-test-api.aircall.io/calls?offset=${0}&limit=${10}`,
    {
      method: "GET",
      headers: {
        "Content-type": "application/json",
        Authorization: `bearer ${gettoken}`,
      },
    }
  );
  const CallsRecord = await response.json();
  return { props: { CallsRecord } };
  }
  else{
    return {
      redirect: {
        permanent: false,
        destination: `/`
      },
    };
  }
};

export default React.memo(CallPage);
