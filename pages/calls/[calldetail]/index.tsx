import { getCookie, hasCookie, setCookie } from "cookies-next"
import type { GetServerSideProps, NextPage } from "next"
import React from "react"
import {CallsDetailPage} from "../../../collection/CallsDetail"
import { CallsDetailProps } from "../../../libs/types/pages"

const CallsDetail: NextPage<CallsDetailProps> = ({CallsRecord,id}) => {
  return (
    <CallsDetailPage  CallsRecord={CallsRecord} id={id}/>
  )
}

export const getServerSideProps: GetServerSideProps=async(context)=> {
  if (hasCookie("UserToken",context)) {
    let gettoken: any =getCookie("UserToken",context)
  gettoken=(JSON.parse(gettoken).token)
const response = await fetch(`https://frontend-test-api.aircall.io/calls/${context?.params?.calldetail}`, {
    method: 'GET',
    headers: {
        'Content-type': 'application/json',
        'Authorization': `bearer ${gettoken}`, // notice the Bearer before your token
    }
})
  const CallsRecord = await response.json()
  return { props: { CallsRecord:CallsRecord,id:context?.params?.calldetail } }
}
else{
  return {
    redirect: {
      permanent: false,
      destination: `/`
    },
  };
}
}


export default React.memo(CallsDetail)