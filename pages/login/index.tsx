import React from 'react'
import Login from '../../collection/Login/Login'
import { NextPage } from 'next';

const LoginPage:NextPage = () => {
  return (
    <>
    <Login/>
    </>
  )
}

export default React.memo(LoginPage)