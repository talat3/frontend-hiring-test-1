import { GetServerSideProps } from "next";
import React, { useEffect } from "react";
import { CallApi } from "../libs/api";
import LoginPage from "./login";

const Home = () => {
  return (
      <LoginPage />
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const isLogedin:any = await CallApi();
  if(isLogedin == false){
    return{ props:{}}
  }
  else{
    return {
      redirect: {
        permanent: false,
        destination: `/calls`
      },
    };
  }
};

export default React.memo(Home);
