import React from 'react'
import { CallsDetailProps } from '../../libs/types/pages'
import CallsDetail from './CallsDetail'

export const CallsDetailPage: React.FC<CallsDetailProps>=({CallsRecord,id})=> {
  return (
    <CallsDetail CallsRecord={CallsRecord} id={id}/>
  )
}
