import styled from "styled-components";
import { Container, Input, Text } from "../../libs/styled-components";
import {
  HeadingProps,
  InputProps,
  TextProps,
} from "../../libs/types/style-components";

export const StyledContainer = styled((props: HeadingProps) => (
  <Container {...props} />
))`
  && {
    margin-top: 10px;
  }
`;

export const StyledText = styled((props: TextProps) => <Text {...props} />)`
  && {
    font-weight: bold;
    margin-left: 20px;
    margin-right: 5px;
  }
`;

export const StyledInput = styled((props: InputProps) => <Input {...props} />)`
  && {
    margin-left: 20px;
    margin-right: 5px;
    width: 30%;
  }
`;