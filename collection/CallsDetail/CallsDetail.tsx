import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Heading, Text } from "../../libs/styled-components";
import { MainHeading } from "../Calls/element";
import helper, { messageType } from "../Helper/helper";
import { StyledText, StyledContainer, StyledInput } from "./element";
import { getCookie, hasCookie } from "cookies-next";
import { CallsDetail, CallsDetailProps, Notes } from "../../libs/types/pages";
import { CallApi } from "../../libs/api";
import router from "next/router";

const CallsDetail: React.FC<CallsDetailProps> = ({ CallsRecord, id }) => {
  const [data, setdata] = useState<CallsDetail>();
  const [note, setNote] = useState<string>("");

  useEffect(() => {
    setdata(CallsRecord);
  }, [CallsRecord]);

  const submit = () => {
    if (helper.isEmptyString(note)) {
      helper.toastNotification("Input is required.", messageType.failed);
    } else {
      if (hasCookie("UserToken")) {
      let gettoken: any = getCookie("UserToken");
      gettoken = JSON.parse(gettoken).token;
      axios
        .post(
          `https://frontend-test-api.aircall.io/calls/${id}/note`,
          {
            content: note,
          },
          {
            headers: {
              Authorization: `bearer ${gettoken}`,
            },
          }
        )
        .then(async (res) => {
          if (res.status == 201) {
            setNote("");
            setdata(res.data);
            helper.toastNotification(
              "Note added Successfully.",
              messageType.success
            );
          } else {
            helper.toastNotification(
              "Unable to process request.",
              messageType.failed
            );
          }
        })
        .catch((error) => {
          helper.toastNotification(
            "Unable to process request.",
            messageType.failed
          );
          console.log(error, "error");
        });
    }
    else{
      router.push('/')
    }
  }
  };

  return (
    <StyledContainer>
      <Heading textalign="center">
        <MainHeading type="h2">Call Detail</MainHeading>
      </Heading>
      {helper.isObject(data) ? (
        <>
          <StyledContainer>
            <StyledText>Call Type:</StyledText>
            <Text>{data?.call_type}</Text>
          </StyledContainer>
          <StyledContainer>
            <StyledText>Created At:</StyledText>
            <Text>{helper.humanReadableDate(data?.created_at)}</Text>
          </StyledContainer>
          <StyledContainer>
            <StyledText>Direction:</StyledText>
            <Text>{data?.direction}</Text>
          </StyledContainer>
          <StyledContainer>
            <StyledText>Duration:</StyledText>
            <Text>{data?.duration}</Text>
          </StyledContainer>
          <StyledContainer>
            <StyledText>From:</StyledText>
            <Text>{data?.from}</Text>
          </StyledContainer>
          <StyledContainer>
            <StyledText>To:</StyledText>
            <Text>{data?.to}</Text>
          </StyledContainer>
          {helper.isArray(data?.notes) && data?.notes.length ? (
            <>
              <StyledContainer>
                <StyledText>Notes</StyledText>
              </StyledContainer>
              {data.notes.map((item: Notes, index: number) => (
                <StyledContainer key={index}>
                  <StyledText>{index + 1}:</StyledText>
                  <Text>{item?.content}</Text>
                </StyledContainer>
              ))}
            </>
          ) : (
            <StyledContainer>
              <StyledText>No Notes Exist For this call</StyledText>
            </StyledContainer>
          )}

          <StyledContainer>
            <StyledInput
              placeholder="Add Notes here..."
              value={note}
              onChange={(e: any) => setNote(e.target.value)}
            />
            <Button onClick={() => submit()}>Add Notes</Button>
          </StyledContainer>
        </>
      ) : (
        ""
      )}
    </StyledContainer>
  );
};
export default React.memo(CallsDetail);
