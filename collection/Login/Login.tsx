import React, { useEffect, useState } from "react";
import {
  Button,
  Form,
  FormItem,
  Image,
  Input,
  PasswordInput,
} from "../../libs/styled-components";
import { useRouter } from "next/router";
import { Header, MainLabel, StyledModal } from "./element";
import { LoginApi } from "../../libs/api";
import { LoginCredentials } from "../../libs/types/pages";

const Login = () => {
  const router = useRouter();
  const [credentials, setcredentials] = useState<boolean>(false)
  const [isModalVisible, setIsModalVisible] = useState(false);

  useEffect(() => {
    // if (getCookie('UserToken') !== 'null' && hasCookie('UserToken')) {
    //   router.push("/calls");
    // }
    // else{
    if(credentials){    
      setIsModalVisible(false);
      router.push('/calls')
    }
    else{
      setIsModalVisible(true);
    }
  // }
  }, [credentials]);

  const onFinish = (values: LoginCredentials) => {
    LoginApi(values, setcredentials)
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <StyledModal visible={isModalVisible} footer={null}>
      <Header textalign="center">
        <Image
          width="100"
          height="50"
          src="/vercel.svg"
          preview={false}
          alt="logo"
        />

        <MainLabel type="h2">Log In</MainLabel>
      </Header>
      <Form layout='vertical' onFinish={onFinish}
      onFinishFailed={onFinishFailed}>
        <FormItem
          label="Username"
          name="username"
          rules={[{ required: true, message: "Please input your username!" }]}
        >
          <Input />
        </FormItem>
        <FormItem
          label="Password"
          name="password"
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <PasswordInput />
        </FormItem>
        <FormItem>
          <Button block htmlType="submit">Log In</Button>
        </FormItem>
      </Form>
    </StyledModal>
  );
};

export default React.memo(Login);
