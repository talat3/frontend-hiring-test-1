import styled from 'styled-components';
import { Heading,Modal,Selecttag} from '../../libs/styled-components';
import { HeadingProps,ModalProps,Selecttagprops } from '../../libs/types/style-components';

export const Header =styled((props:HeadingProps)=>(
    <Heading {...props}/>
))`
margin-top:20px;
`
export const MainLabel =styled((props:Selecttagprops)=>(
    <Selecttag {...props}/>
))`
margin-top:40px;
`

export const StyledModal =styled((props:ModalProps)=>(
    <Modal {...props}/>
))`
`