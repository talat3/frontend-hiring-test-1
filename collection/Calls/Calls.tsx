import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import axios from "axios";
import helper, { messageType } from "../Helper/helper";
import {
  Container,
  Heading,
  Switch,
  Table,
  Tooltip,
} from "../../libs/styled-components";
import { EyeIcon, MainHeading, StyledPagination } from "./element";
import { ColumnsType } from "antd/es/table";
import {
  CallPageProps,
  DataType,
  pagestate,
  CallsDetail,
} from "../../libs/types/pages";
import {
  setCookie,
  getCookie,
  deleteCookie,
  hasCookie,
  CookieValueTypes,
} from "cookies-next";

const Calls: React.FC<CallPageProps> = ({ CallsRecord }) => {
  const route = useRouter();
  const [data, setdata] = useState<CallsDetail[]>([]);
  const [pages, setpages] = useState<pagestate>({
    currentpage: 1,
    totalpages: 1,
    countPerPage: 10,
    offset: 0,
  });

  useEffect(() => {
    if (CallsRecord?.statusCode == 401 || helper.isEmptyString(CallsRecord)) {
      route.push("/");
    } else {
      sortAscending(CallsRecord?.nodes);
      setpages({
        currentpage: 1,
        totalpages: CallsRecord?.totalCount,
        countPerPage: 10,
        offset: 1,
      });
    }

    setInterval(() => {
      refreshToken();
    }, 540000);
  }, []);

  const getCallData = (e: number) => {
    if (hasCookie("UserToken")) {
    let gettoken: any = getCookie("UserToken");
    gettoken = JSON.parse(gettoken).token;
    axios
      .get(
        `https://frontend-test-api.aircall.io/calls?offset=${
          (e - 1) * 10
        }&limit=${pages.countPerPage}`,
        {
          headers: {
            Authorization: `bearer ${gettoken}`,
          },
        }
      )
      .then(async (res) => {
        if (res.status == 200) {
          sortAscending(res.data.nodes);
          setpages({
            currentpage: e,
            totalpages: res.data.totalCount,
            countPerPage: 10,
            offset: (e - 1) * 10,
          });
          helper.toastNotification("Data Receive.", messageType.success);
        } else {
          helper.toastNotification(
            "Unable to process request.",
            messageType.failed
          );
        }
      })
      .catch((error) => {
        helper.toastNotification("Login Again.", messageType.failed);
        deleteCookie("UserToken");
        route.push("/");
        console.log(error, "error");
      });
    }
    else{
      route.push("/");
    }
  };

  const refreshToken = () => {
    if (hasCookie("UserToken")) {
      let gettoken: any = getCookie("UserToken");
      gettoken = JSON.parse(gettoken).token;
      axios
        .post(
          `https://frontend-test-api.aircall.io/auth/refresh-token`,
          {},
          {
            headers: {
              Authorization: `bearer ${gettoken}`,
            },
          }
        )
        .then(async (res) => {
          if (res.status == 201) {
            let authToken = { token: res.data.access_token };
            setCookie("UserToken", JSON.stringify(authToken));
            helper.toastNotification(
              "Token Updated Successfully.",
              messageType.success
            );
          } else {
            helper.toastNotification(
              "Unable to process request.",
              messageType.failed
            );
          }
        })
        .catch((error) => {
          helper.toastNotification(
            "Unable to process request.",
            messageType.failed
          );
          deleteCookie("UserToken");
          route.push("/");
          console.log(error, "error");
        });
    }
    else{
      route.push('/')
    }
  };

  const sortAscending = (data: any) => {
    setdata(
      data.sort((a: any, b: any) => (a.created_at > b.created_at ? -1 : 1))
    );
  };

  const archived = (e: boolean, index: number, item: CallsDetail) => {
    let array: CallsDetail[] = [...data];
    array[index].is_archived = e;
    setdata(array);
    if (hasCookie("UserToken")) {
    let gettoken: any = getCookie("UserToken");
    gettoken = JSON.parse(gettoken).token;
    axios
      .put(
        `https://frontend-test-api.aircall.io/calls/${item.id}/archive`,
        { item },
        {
          headers: {
            Authorization: `bearer ${gettoken}`,
          },
        }
      )
      .then(async (res) => {
        if (res.status == 200) {
          helper.toastNotification(
            "Updated Successfully.",
            messageType.success
          );
        } else {
          let array: CallsDetail[] = [...data];
          array[index].is_archived = !array[index].is_archived;
          setdata(array);
          helper.toastNotification("Call Doesnot Exist.", messageType.failed);
        }
      })
      .catch((error) => {
        let array: CallsDetail[] = [...data];
        array[index].is_archived = !array[index].is_archived;
        setdata(array);
        helper.toastNotification("Call Doesnot Exist.", messageType.failed);
        console.log(error, "error");
      });
    }
    else {
      route.push('/')
    }
  };

  const columns: ColumnsType<DataType> = [
    {
      title: "#",
      render: (item, record, index) => index + 1,
    },
    {
      title: "Call Type",
      dataIndex: "call_type",
    },
    {
      title: "Number",
      dataIndex: "from",
    },
    {
      title: "Direction",
      dataIndex: "direction",
    },
    {
      title: "Date/Time",
      dataIndex: "created_at",
      render: (text) => helper.humanReadableDate(text),
    },
    {
      title: "Actions",
      dataIndex: "is_archived",
      render: (text, item: any, index): any => (
        <>
          <Tooltip title={text ? "Archived" : "Un Archived"}>
            <Switch checked={text} onChange={(e) => archived(e, index, item)} />
          </Tooltip>
          <Tooltip title="Call Detail">
            <EyeIcon onClick={(e: any) => route.push("/calls/" + item.id)} />
          </Tooltip>
        </>
      ),
    },
  ];
  return (
    <Container>
      <Heading textalign="center">
        <MainHeading type="h2">Calls</MainHeading>
      </Heading>
      <Table columns={columns} dataSource={data} pagination={false} />
      <StyledPagination
        current={pages.currentpage}
        onChange={(e) => getCallData(e)}
        total={pages.totalpages}
        hideOnSinglePage
      />
    </Container>
  );
};

export default React.memo(Calls);
