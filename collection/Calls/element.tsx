import styled from "styled-components";
import { Pagination, Selecttag } from "../../libs/styled-components";
import {
  PaginationProps,
  Selecttagprops,
} from "../../libs/types/style-components";
import { EyeOutlined } from "@ant-design/icons";

export const MainHeading = styled((props: Selecttagprops) => (
  <Selecttag {...props} />
))`
  && {
    text-align: center;
    margin-top: 20px;
  }
`;
export const StyledPagination = styled((props: PaginationProps) => (
  <Pagination {...props} />
))`
  margin-top: 10px;
  float: right;
  .ant-pagination-options {
    display: none;
  }
`;

export const EyeIcon = styled((props: any) => <EyeOutlined {...props} />)`
  font-size: 20px;
  margin-left: 15px;
  cursor: pointer;
`;
