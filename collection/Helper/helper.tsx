import { toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

export enum messageType{
    success="SUCCESS_MESSAGE",
    failed="Failed_MESSAGE"
}

const helper = {

    toastNotification(message:string, type:string) {
        toast(message, {
            type: type === messageType.success ? "success" : "error",
            autoClose: 4000,
            className:
                type === messageType.success ? 'toastContainerSuccess' : 'toastContainer'
        })
    },
    isEmptyString(str:any) {
        if (str === '' || str === undefined || str === null) {
            return true
        }
        return false
    },
    humanReadableDate(timestamp = '') {
        if (timestamp) {
            const time = timestamp.replace(' 00:00:00', '')
            let a:string | string[] = new Date(time).toUTCString()
            a = a.split('GMT')
            return a[0]
        } else {
            let a:string | string[] = new Date().toUTCString()
            a = a.split('GMT')
            return a[0]
        }
    },
    isObject(value:any) {
        if (value === null || value === undefined || value === "") {
            return false;
        }
        else if (Object.keys(value).length !== 0) {
            return true;
        }
        else {
            return false;
        }
    },
    isArray(data:any) {
        try {
            if (data.length)
                return true
            else
                return false
        } catch (e) {
            return false;
        }
    },
}

export default helper