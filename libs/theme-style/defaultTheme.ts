import { Theme } from "../types/style-components";

export const defaultTheme:Theme ={
    backgroundColor:'#000',
    color:'#fff',
    borderColor:'#000',
    background:'#000',
}