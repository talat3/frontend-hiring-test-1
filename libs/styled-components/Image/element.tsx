import styled from "styled-components";
import { Image as AntImage} from 'antd';
import { Imageprops } from "../../types/style-components";

export const Image :React.FC<Imageprops> = styled((props: Imageprops)=> {
return <AntImage {...props}/>
})`
`;