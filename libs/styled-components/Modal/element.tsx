import styled from "styled-components";
import { Modal as AntModal } from "antd";
import { ModalProps } from "../../types/style-components";

export const Modal: React.FC<ModalProps> = styled((props: ModalProps) => {
  return <AntModal {...props} />;
})``;
