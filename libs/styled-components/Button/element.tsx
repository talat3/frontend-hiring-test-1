import styled from "styled-components";
import { Button as AntButton } from "antd";
import { Buttonprops } from "../../types/style-components";
import { defaultTheme } from "../../theme-style/defaultTheme";

export const Button: React.FC<Buttonprops> = styled((props: Buttonprops) => {
  return <AntButton {...props} />;
})`
background-color:${defaultTheme.backgroundColor};
color:${defaultTheme.color};
`;
