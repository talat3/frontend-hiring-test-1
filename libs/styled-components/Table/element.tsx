import styled from "styled-components";
import { Table as AntTable } from 'antd';
import { omitCSSProps, TableProps } from "../../types/style-components";

export const Table = styled((props: TableProps) => {
    const domProps = omitCSSProps(props);
    return <AntTable {...domProps} />;
  })`
  margin-left:10px;
  `