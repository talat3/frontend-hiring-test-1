import styled from "styled-components";
import { TextProps } from "../../types/style-components";

export const Text :React.FC<TextProps> = styled((props: TextProps)=> <span {...props}/>)`
`;