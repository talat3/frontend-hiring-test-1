import styled from "styled-components";
import { Pagination as AntPagination} from 'antd';
import { PaginationProps } from "../../types/style-components";

export const Pagination = styled((props: PaginationProps) => {
  return <AntPagination {...props} />;
})`
`;