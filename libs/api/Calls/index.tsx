import axios from "axios";
import { getCookie, hasCookie } from "cookies-next";

export const CallApi = async () => {
  try {
    if (hasCookie("UserToken")) {
      let gettoken: any = getCookie("UserToken");
      gettoken = JSON.parse(gettoken).token;
      const { data: response } = await axios.get(
        `https://frontend-test-api.aircall.io/calls?offset=${0}&limit=${10}`,
        {
          headers: {
            Authorization: `bearer ${gettoken}`,
          },
        }
      );
      return response;
    } else {
      return false;
    }
  } catch (error) {
    return false;
  }
};
