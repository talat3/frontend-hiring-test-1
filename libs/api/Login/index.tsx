import axios from "axios";
import helper, { messageType } from "../../../collection/Helper/helper";
import { setCookie } from 'cookies-next';
import { LoginCredentials } from "../../types/pages";

export const LoginApi = async(credentials:LoginCredentials, setcredentials: React.Dispatch<React.SetStateAction<boolean>>) =>{
    
    return await axios
      .post(`https://frontend-test-api.aircall.io/auth/login`, credentials)
      .then((res) => {
        if (res.status == 201) {
          let authToken ={token:res.data.access_token}
          setCookie("UserToken", JSON.stringify(authToken));
          helper.toastNotification("Login Successfully.", messageType.success);
          setcredentials(true)
        } else {
          helper.toastNotification(
            "Unable to process request.",
            messageType.failed
          );
        }
      })
      .catch((error) => {
        helper.toastNotification(
          "Unable to process request.",
          messageType.failed
        );
        console.log(error, "error");
      });
}