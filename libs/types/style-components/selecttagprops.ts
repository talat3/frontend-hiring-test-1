import { DetailedHTMLProps, HTMLAttributes } from "react";

export interface Selecttagprops extends DetailedHTMLProps<HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>{
    type:string
}