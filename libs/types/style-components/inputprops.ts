import {
    InputProps as AntInputProps,
    PasswordProps as AntInputPasswordProps
  } from 'antd/es/input';
  
  import { BoxProperties } from './css';
  
  export type InputProps=any;
//     extends Omit<AntInputProps, 'width' | 'height'>,
//       BoxProperties {
//     ref?: React.Ref<HTMLInputElement>;
//   }
  
  export type InputPasswordProps = AntInputPasswordProps & BoxProperties;
  