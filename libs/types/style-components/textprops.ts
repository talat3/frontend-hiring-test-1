import { Ref } from 'react';
import { BoxProperties } from './css';

type SpanElProps = JSX.IntrinsicElements['span'];
type TextType = 'extra-large' | 'large' | 'body' | 'small' | 'tiny';
export interface TextProps extends BoxProperties, SpanElProps {
  ref?: Ref<HTMLSpanElement>;
  overflow?: string;
  kind?: TextType;
}