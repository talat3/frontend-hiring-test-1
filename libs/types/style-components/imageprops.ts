

export interface Imageprops{
    width?:string,
    height?:string,
    src?:string, 
    preview?:boolean,
    alt?:string
}