import { SwitchProps as AntSwitchProps } from 'antd/es/switch';

import { BoxProperties } from './css';

export type SwitchProps = AntSwitchProps & BoxProperties;