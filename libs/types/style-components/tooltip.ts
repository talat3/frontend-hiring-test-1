import { TooltipProps as AntTooltipProps } from 'antd/es/tooltip';

export type TooltipProps = AntTooltipProps;