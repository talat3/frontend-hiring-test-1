import { TableProps as AntTableProps } from 'antd/es/table';

export type TableProps = AntTableProps<any>;
