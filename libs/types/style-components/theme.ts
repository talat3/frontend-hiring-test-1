export interface Theme {
    backgroundColor:string,
    color:string,
    borderColor:string,
    background:string,
}